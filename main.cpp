#include <fstream>
#include <string>
#include <random>
#include <ctime>
#include <vector>
#include "sleepy_discord/websocketpp_websocket.h"
#include "lewd.hpp"
#include <unordered_map>

std::string getToken()
{
	std::ifstream ifs("token.txt");
	std::string token;
	ifs >> token;
	return token;
}

class Shuffler
{
	std::mt19937 mt;
	unsigned int maxmessages = 75;
	unsigned int minmessages = 15;
	long m_count;
	void reroll()
	{
		m_count = mt()%(maxmessages-minmessages) + minmessages;
	}
public:
	Shuffler() : mt(time(NULL))
	{
		std::ifstream ifs("config.txt");
		if(ifs.is_open())
		{
			ifs >> minmessages >> maxmessages;
		}
		reroll();
	}
	bool count()
	{
		if(--m_count <= 0)
		{
			reroll();
			return true;
		}
		return false;
	}
	void changeMax(const unsigned int max)
	{
		maxmessages = max;
		reroll();
	}
	void changeMin(const unsigned int min)
	{
		minmessages = min;
		reroll();
	}
};

class Quoter
{
	std::mt19937 mt;
	std::vector<std::string> quotes;
public:
	Quoter() : mt(time(NULL))
	{
		std::ifstream ifs("quotes.txt");
		std::string input;
		while(std::getline(ifs, input))
		{
			quotes.push_back(std::move(input));
		}
	}
	const std::string& getRandomQuote()
	{
		return quotes[mt()%quotes.size()];
	}
	void add_quote(const std::string &quote)
	{
		quotes.push_back(quote);
		std::ofstream ofs("quotes.txt", std::ios::trunc);
		for(const auto &quote : quotes)
		{
			ofs << quote << '\n';
		}
	}
	std::string getAllQuotes() const
	{
		std::string result = "```\\n";
		for(size_t i = 0; i < quotes.size(); ++i)
		{
			result += std::to_string(i);
			result += ": ";
			result += quotes[i];
			result += "\\n";
		}
		result += "```";
		return result;
	}
	void removeQuote(size_t entry)
	{
		if(entry >= quotes.size())
		{
			throw std::out_of_range("Not in range input is");
		}
		quotes.erase(quotes.begin()+entry);
		std::ofstream ofs("quotes.txt", std::ios::trunc);
		for(const auto &quote : quotes)
		{
			ofs << quote << '\n';
		}
	}
};

class MessageHandler
{
	const std::string &message;
	std::string m_result;
public:
	MessageHandler(const std::string &message) : message(message) {}
	bool startsWith(const std::string &test)
	{
		if(message.compare(0, test.length(), test) == 0)
		{
			m_result = std::string(message.begin()+test.length(), message.end());
			return true;
		}
		return false;
	}
	const std::string &result() const
	{
		return m_result;
	}
};

class MyClientClass : public SleepyDiscord::DiscordClient
{
	Shuffler shuffle;
	Quoter quoter;
	Lewder lewder;
	std::string findAndExtract(const std::string &input, const std::string &searched)
	{
		if(input.compare(0, searched.length(), searched))
		{
			return std::string(input.begin() + searched.length(), input.end());
		}
		return std::string();
	}
	void handleCommands(const SleepyDiscord::Message &message)
	{
		MessageHandler handler(message.content);
		try
		{
			if(handler.startsWith("Yoda add quote "))
			{
				quoter.add_quote(handler.result());
				sendMessage(message.channelID, "Added quote");
			}
			else if(handler.startsWith("Yoda remove quote "))
			{
				quoter.removeQuote(std::stoi(handler.result()));
				sendMessage(message.channelID, "Quote removed");
			}
			else if(handler.startsWith("Yoda change max "))
			{
				shuffle.changeMax(std::stoi(handler.result()));
				sendMessage(message.channelID, "Changed max random number");
			}
			else if(handler.startsWith("Yoda change min "))
			{
				shuffle.changeMin(std::stoi(handler.result()));
				sendMessage(message.channelID, "Changed min random number");
			}
			else if(message.content == "Yoda list quotes")
			{
				sendMessage(message.channelID, quoter.getAllQuotes());
			}
			else if(message.content == "Yoda enable lewd")
			{
				if(getChannel(message.channelID).cast().isNSFW)
				{
					lewder.lewdChannel(message.channelID);
					sendMessage(message.channelID, "Lewding enabled");
				}
			}
			else if(message.content == "Yoda disable lewd")
			{
				lewder.unlewdChannel(message.channelID);
				sendMessage(message.channelID, "Lewding disabled");
			}
			else if(handler.startsWith("Yoda send lewd"))
			{
				if(lewder.isChannelLewded(message.channelID))
				{
					std::string tags;
					if(!handler.result().empty())
					{
						tags = std::string(handler.result().begin()+1, handler.result().end());
					}
					if(!lewder.isChannelPorn(message.channelID))
					{
						tags += " rating:safe";
					}
					try
					{
						sendMessage(message.channelID, lewder.getRandomLewd(tags));
					}
					catch(...)
					{
						sendMessage(message.channelID, "Nothing found");
					}
				}
				else
				{
					sendMessage(message.channelID, "Show lewds I will not, enabled I am not");
				}
			}
			else if(message.content == "Yoda enable porn")
			{
				if(!lewder.isChannelLewded(message.channelID))
				{
					sendMessage(message.channelID, "Enable lewds first");
				}
				else
				{
					lewder.enablePorn(message.channelID);
					sendMessage(message.channelID, "Porn enabled");
				}
			}
			else if(message.content == "Yoda disable porn")
			{
				lewder.disablePorn(message.channelID);
				sendMessage(message.channelID, "To safety back we are");
			}
		}
		catch(std::invalid_argument&)
		{
			sendMessage(message.channelID, "Invalid argument provided have you");
		}
		catch(...)
		{
			sendMessage(message.channelID, "Something went seriously wrong");
		}
	}
public:
	using SleepyDiscord::DiscordClient::DiscordClient;
	void onMessage(SleepyDiscord::Message message) override final
	{
		if(message.author.bot)
		{
			return;
		}
		if(message.startsWith("Yoda "))
		{
			handleCommands(message);
			return;
		}
		if(message.content == "ping")
		{
			const std::string mention = "Hello, <@" + message.author.ID.string() + '>';
			auto start = std::chrono::steady_clock::now();
			auto temp = sendMessage(message.channelID, mention + "> (pinging...)");
			auto end = std::chrono::steady_clock::now();
			editMessage(temp, mention + " (" + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) + " ms)");
		}
		if(shuffle.count())
		{
			sendMessage(message.channelID, quoter.getRandomQuote());
		}
	}
};

int main()
{
	MyClientClass client(getToken(), 2);
	client.run();
}
