#pragma once
#include <unordered_set>
#include <string>
#include <curl/curl.h>
#include <random>

class Lewder
{
	CURL* curl;
	std::mt19937_64 mt;
	std::unordered_set<std::string> enabledChannels;
	std::unordered_set<std::string> explicitChannels;
	void updateEnabled();
	void updateExplicit();
public:
	Lewder();
	~Lewder();
	void lewdChannel(const std::string &channelID);
	void unlewdChannel(const std::string &channelID);
	bool isChannelLewded(const std::string &channelID) const;
	void enablePorn(const std::string &channelID);
	void disablePorn(const std::string &channelID);
	bool isChannelPorn(const std::string &channelID) const;
	std::string getRandomLewd(std::string tags);
};
