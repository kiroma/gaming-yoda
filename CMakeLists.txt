cmake_minimum_required(VERSION 3.12)
project(yoda-gaming CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(JSON_BuildTests OFF CACHE INTERNAL "")

find_package(CURL REQUIRED)
add_subdirectory(sleepy-discord)
add_subdirectory(json)

add_executable(yoda main.cpp lewder.cpp)

target_link_libraries(yoda PRIVATE sleepy-discord nlohmann_json::nlohmann_json CURL::libcurl)
