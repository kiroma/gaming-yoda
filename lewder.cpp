#include "lewd.hpp"
#include <fstream>
#include <nlohmann/json.hpp>

Lewder::Lewder() : mt([](){std::random_device rd; return rd();}())
{
	curl = curl_easy_init();
	std::ifstream ifs("lewds.txt", std::ios::binary);
	if(!ifs.is_open())
	{
		return;
	}
	std::string input;
	while(ifs >> input)
	{
		enabledChannels.emplace(input);
	}
	ifs.open("porn.txt", std::ios::binary);
	if(!ifs.is_open())
	{
		return;
	}
	while(ifs >> input)
	{
		explicitChannels.emplace(input);
	}
}

Lewder::~Lewder()
{
	curl_easy_cleanup(curl);
}

void Lewder::updateEnabled()
{
	std::ofstream ofs("lewds.txt", std::ios::trunc | std::ios::binary);
	for(const std::string &a : enabledChannels)
	{
		ofs << a << '\n';
	}
}

void Lewder::lewdChannel(const std::string& channelID)
{
	enabledChannels.emplace(channelID);
	updateEnabled();
}

void Lewder::unlewdChannel(const std::string& channelID)
{
	enabledChannels.erase(channelID);
	updateEnabled();
}

bool Lewder::isChannelLewded(const std::string& channelID) const
{
	return enabledChannels.find(channelID) != enabledChannels.end();
}

void Lewder::updateExplicit()
{
	std::ofstream ofs("porn.txt", std::ios::trunc | std::ios::binary);
	for(const std::string &a : explicitChannels)
	{
		ofs << a << '\n';
	}
}

void Lewder::enablePorn(const std::string& channelID)
{
	explicitChannels.emplace(channelID);
	updateExplicit();
}

void Lewder::disablePorn(const std::string& channelID)
{
	explicitChannels.erase(channelID);
	updateExplicit();
}

bool Lewder::isChannelPorn(const std::string& channelID) const
{
	return explicitChannels.find(channelID) != explicitChannels.end();
}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	static_cast<std::string*>(userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

std::string Lewder::getRandomLewd(std::string tags)
{
	for(char &a : tags)
	{
		if(a == ' ')
		{
			a = '+';
		}
	}
	std::string readBuffer;
	curl_easy_setopt(curl, CURLOPT_URL, "https://e621.net/post/index.json?limit=1");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "Gaming Yoda by kiroma");
	curl_easy_perform(curl);
	auto response = nlohmann::json::parse(readBuffer);
	std::uniform_int_distribution<std::mt19937::result_type> dis(1, response[0]["id"]);
	std::string fullurl = "https://e621.net/post/index.json?limit=1&before_id=" + std::to_string(dis(mt));
	if(!tags.empty())
	{
		fullurl += "&tags=" + tags;
	}
	curl_easy_setopt(curl, CURLOPT_URL, fullurl.c_str());
	readBuffer.clear();
	curl_easy_perform(curl);
	response = nlohmann::json::parse(readBuffer);
	if(response.size())
	{
		return response[0]["file_url"];
	}
	readBuffer.clear();
	fullurl = "https://e621.net/post/index.json?limit=1";
	if(!tags.empty())
	{
		fullurl += "&tags=" + tags;
	}
	curl_easy_setopt(curl, CURLOPT_URL, fullurl.c_str());
	curl_easy_perform(curl);
	if(response.size())
	{
		return response[0]["file_url"];
	}
	return "Could not find anything";
}
